# katapotter

Summary:
The solution aims to determine the best way to purchase books from the Harry Potter series while benefiting from the most advantageous discounts. The goal is to maximize savings by optimally grouping the books.

Overall Strategy:

Combination Management: When adding a book to the basket, the solution checks if this book is already included in an existing combination. If it is, a new combination is created for this book. If the book isn't already present, it's added to an existing combination to maximize the discount.

Discount Optimization: The solution evaluates all possible combinations to determine the best way to add a book in order to minimize the total cost. It takes into account the available discounts for different groups of books to achieve this optimization.

Recursive Approach: To process each book, the solution uses a recursive approach, allowing the problem to be broken down into smaller, more manageable sub-problems.
