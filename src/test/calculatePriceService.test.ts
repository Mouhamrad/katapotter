import {
  calculateTotalPrice,
  computeDiscountedPriceForGroup,
  BASE_PRICE,
} from "../application/calculatePriceService";

describe("calculateTotalPrice", () => {
  it("should return 0 for an empty basket", () => {
    expect(calculateTotalPrice([])).toBe(0);
  });

  it("calculates base price for a single book", () => {
    expect(calculateTotalPrice([1])).toBe(BASE_PRICE);
  });

  it("should return appropriate cost for multiple identical books", () => {
    expect(calculateTotalPrice([1, 1, 1])).toBe(BASE_PRICE * 3);
  });

  it("should apply 5% discount for two different books", () => {
    const expectedPrice = 2 * BASE_PRICE * 0.95;
    expect(calculateTotalPrice([1, 2])).toBe(expectedPrice);
  });

  it("should apply 10% discount for three different books", () => {
    const expectedPrice = 3 * BASE_PRICE * 0.9;
    expect(calculateTotalPrice([1, 2, 3])).toBe(expectedPrice);
  });

  it("should apply 20% discount for four different books", () => {
    const expectedPrice = 4 * BASE_PRICE * 0.8;
    expect(calculateTotalPrice([1, 2, 3, 4])).toBe(expectedPrice);
  });

  it("should apply 25% discount for five different books", () => {
    const expectedPrice = 5 * BASE_PRICE * 0.75;
    expect(calculateTotalPrice([1, 2, 3, 4, 5])).toBe(expectedPrice);
  });

  it("should return the optimal price for a complex basket #1", () => {
    const basket = [1, 1, 2, 2, 3, 3, 4, 5];
    const expectedPrice = 2 * computeDiscountedPriceForGroup(4);
    expect(calculateTotalPrice(basket)).toEqual(expectedPrice);
  });

  it("should return the optimal price for a complex basket #2", () => {
    const basket = [1, 1, 1, 2, 2, 3, 3, 4, 4, 5, 5];
    const expectedPrice =
      2 * computeDiscountedPriceForGroup(5) + computeDiscountedPriceForGroup(1);
    expect(calculateTotalPrice(basket)).toEqual(expectedPrice);
  });
});
