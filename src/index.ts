import { calculateTotalPrice } from "./application/calculatePriceService";

function main() {
  const Basket = [1, 1, 2, 2, 3, 3, 4, 5];

  const totalPriceOne = calculateTotalPrice(Basket);
  console.log("The total price :", totalPriceOne);
}

main();

export { main };
