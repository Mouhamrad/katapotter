type DiscountMap = {
  [key: number]: number;
};

// Map of discounts for the number of unique books.
const DISCOUNTS: DiscountMap = {
  1: 1,
  2: 0.95,
  3: 0.9,
  4: 0.8,
  5: 0.75,
};

export const BASE_PRICE = 8;

// Calculates the total price for a set of books.
export function calculateTotalPrice(books: number[]): number {
  return determineDiscountGroups(books).reduce((price, group) => {
    return price + computeDiscountedPriceForGroup(group.length);
  }, 0);
}

// Determines optimal discount groups for the given set of books.
function determineDiscountGroups(books: number[]): number[][] {
  return books.reduce(aggregateGroups, []);
}

// Aggregates books into optimal discount groups.
function aggregateGroups(groups: number[][], book: number): number[][] {
  const optimalGroup = findOptimalGroupForBook(book, groups);

  if (!optimalGroup.length) {
    groups.push(optimalGroup);
  }

  optimalGroup.push(book);
  return groups;
}

// Finds the best discount group for a given book.
function findOptimalGroupForBook(book: number, groups: number[][]): number[] {
  const eligibleGroups = groups.filter((group) => !group.includes(book));
  return eligibleGroups.reduce(selectMoreSuitableGroup, []);
}

// Selects the group where the book addition would result in the most discount.
function selectMoreSuitableGroup(group1: number[], group2: number[]): number[] {
  const discountDifferenceGroup1 = priceDifferenceForAddingBook(group1);
  const discountDifferenceGroup2 = priceDifferenceForAddingBook(group2);

  return discountDifferenceGroup1 > discountDifferenceGroup2 ? group1 : group2;
}

// Calculates how much more discount can be achieved by adding a book to a group.
function priceDifferenceForAddingBook(group: number[]): number {
  const currentPrice = computeDiscountedPriceForGroup(group.length);
  const newPrice = computeDiscountedPriceForGroup(group.length + 1);

  return currentPrice - newPrice;
}

// Computes the discounted price for a group of books.
export function computeDiscountedPriceForGroup(length: number): number {
  const undiscountedPrice = length * BASE_PRICE;
  const discountFactor = DISCOUNTS[length] || 1;
  return undiscountedPrice * discountFactor;
}
